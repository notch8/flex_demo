# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

u = User.where(email: 'hyrax@example.com').first_or_create do |user|
  user.password = 'testing123'
end
u.roles << Role.where(name: 'admin').first_or_create
u.save!
puts "========= #{u.email} ~ created or found"

Rake::Task['hyrax:default_collection_types:create'].invoke
Rake::Task['hyrax:default_admin_set:create'].invoke
