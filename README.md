# README

Welcome to our demo of AllinsonFlex. In order to get prepared for the workshop, please clone the repo

`git clone https://gitlab.com/notch8/flex_demo.git`

Then from inside the checkout run `docker-compose pull`

That will get you the baseline items you'll need for our day of work.
